## SNT - Localisation, cartographie et mobilité

### Compétences attendues

| Contenus              | Capacités attendues                                          |
| --------------------- | ------------------------------------------------------------ |
| GPS, Galileo          | Décrire le principe de fonctionnement de la géolocalisation. |
| Cartes numériques     | Identifier les différentes couches d’information de GéoPortail pour extraire différents types de données. |
|                       | Contribuer à OpenStreetMap de façon collaborative.           |
| Protocole NMEA 0183   | Décoder une trame NMEA pour trouver des coordonnées géographiques. |
| Calculs d’itinéraires | Utiliser un logiciel pour calculer un itinéraire.            |
|                       | Représenter un calcul d’itinéraire comme un problème sur un graphe. |
| Confidentialité       | Régler les paramètres de confidentialité d’un téléphone pour partager ou non sa position. |

------

Thème **transversal**, le thème Localisation, cartographie fait le Lien avec 

- le HTML, carte au format html (Thème : WEB)
- les graphes, calcul d'itinéraires (Thème : réseau sociaux)
- les données géolocalisées (Thème :  Données structurées et Thème photo avec les métadonnées exif)

------------------

### Séquences :

#### [1 Géolocalisation, cartographie : présentation](1_géolocalisation_cartographie/localisation-cartographie-01.md)

#### [2 Cartographie, représentation des données](2_cartographie_représentation_données/localisation-cartographie-02.md)

#### [3 Cartographie et itinéraires](3_cartographie_itinéraires/localisation-cartographie-03.md)

#### [4 Protocole NMEA](4_protocole_NMEA/localisation-cartographie-04.md)



### 🌎 Fil rouge

<p style='background-color : red;font-size:2px;text-align : center' >.</p>
Vous devez créer des cartes numériques avec des données géolocalisées pour faire la promotion d'un lieu de votre choix.

Dans chacune des 3 Séquences (1,2,3), remplacer le lieu proposé (souvent Boulogne sur mer) par ce lieu que vous avez choisi.

Les cartes générées (format HTML) devront être copiées dans un dossier.

Elles seront alors automatiquement intégrées dans une page HTML (comprenant des iframes) pour une présentation rapide.

[en savoir plus fil rouge](localisation-cartographie-fil-rouge.md)

### Mémento

------

**Coordonnées géographiques :** système de trois éléments (longitude, latitude, altitude) permettant de repéré un point de la Terre.

**GPS :**  Le ***Global Positioning System*** **(GPS)**, en français : « Système mondial de positionnement » [littéralement] ou « Géo-positionnement par satellite »

**Cartes numériques :** fichiers numériques comportant des informations de cartographie provenant de sources variées (données des services géographiques des États, photos prises par des satellites, avions ou voitures, données fournies par les utilisateurs...). Ces informations sont de natures diverses : topographiques, géologiques, photographiques, liées aux transports, à l’activité industrielle ou touristique...

**Géoportail** : portail Web public français permettant l'accès à des services de recherche et de
visualisation de données géographiques ou géolocalisées de l’ensemble du territoire français. Mis en
œuvre par deux établissements publics, l’IGN et le BRGM, il a été inauguré en 2006. Doté d’une très
large palette multicouche d’informations et de données publiques, il est non modifiable mais permet toutefois de créer des contenus personnels.

**Openstreetmap (OSM)** : projet de cartographie mondial, né en 2004 en Angleterre, libre et collaboratif, utilisant le système GPS et d'autres données libres. Il est multicouche mais sans imagerie satellitaire.

La **norme NMEA 0183** est une spécification pour la communication entre équipements marins,
dont les équipements GPS. Elle est définie et contrôlée par la National Marine Electronics Association
basée à Severna Park au Maryland (États-Unis d'Amérique).

### Références et liens

------

**Géolocalisation**

https://fr.wikipedia.org/wiki/G%C3%A9olocalisation

https://fr.wikipedia.org/wiki/Information_g%C3%A9ographique#Articles_connexes

https://fr.wikipedia.org/wiki/Liste_de_projections_cartographiques

https://fr.wikipedia.org/wiki/Global_Positioning_System

GPS : https://youtu.be/WoqpQbWdacQ    provient de la chaîne YouTube [Unisciel](https://www.youtube.com/channel/UCK2qTIRbiBg7fsZbt1lj6RA)

Galiléo : https://youtu.be/e79tSIpLiDk        provient de la chaîne YouTube [CNES]

Confidentialité Google :  https://maps.google.com/locationhistory/

Quand les marques vous suivent à la trace  [Tout compte fait](https://www.youtube.com/watch?v=rhgNhd0fUl4) (France 2)

[fonctionnement des systèmes de géolocalisation des smartphones](https://fr.organilog.com/454-fonctionnement-geolocalisation-mobile/)



**Itinéraires**

 https://maps.openrouteservice.org/

algorithme de Dijkstra. https://www.youtube.com/watch?v=MybdP4kice4

[Algorithme de Dijkstra](https://youtu.be/JPeCmKFrKio)

**Cartographie**

https://fr.wikipedia.org/wiki/Cartographie

https://www.geoportail.gouv.fr

https://www.openstreetmap.org

**Géomatique**

https://fr.wikipedia.org/wiki/Portail:Information_g%C3%A9ographique

https://fr.wikipedia.org/wiki/G%C3%A9omatique

**Formats de données géolocalisable**

https://geojson.org/

http://geojson.io

https://fr.wikipedia.org/wiki/GPX_(format_de_fichier)

https://fr.wikipedia.org/wiki/Keyhole_Markup_Language

**Données structurées géographiques** aux formats geojson et csv : source INSEE sur data.gouv.fr

https://www.data.gouv.fr/fr/search/?q=communes+pas+de+calais

https://www.data.gouv.fr/fr/datasets/population-legale-en-2011-dans-le-departement-du-pas-de-calais/

https://www.data.gouv.fr/fr/datasets/geozones/

https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/

Base de données [sirène 3](https://data.opendatasoft.com/explore/dataset/sirene_v3%40public/map/?disjunctive.libellecommuneetablissement&disjunctive.etatadministratifetablissement&disjunctive.sectionetablissement&disjunctive.naturejuridiqueunitelegale&sort=datederniertraitementetablissement&q=pas+de+calais+th%C3%A9atre&location=13,50.73015,1.60315&basemap=jawg.sunny) : les entreprises sur le territoire français

------

Publié sous licence CC-BY-SA
