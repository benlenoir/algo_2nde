---
title: "Le principe des fonctions sous Python"
author: B. Lenoir
date: "13/02/2020"
subject: "Markdown"
keywords: [Markdown, Example]
lang: "fr"
book: True
mainfont: times
documentclass: article
geometry:
- hmargin=10mm
- vmargin=10mm
- heightrounded
---

# Présentation du concept

Python permet de créer des fonctions dont le principe est similaire à celui d'une fonction mathématique. On peut les considérer comme des petits programmes dans les programmes.

On donne à la fonction une entrée - aussi appelée paramètre - et la fonction renvoie quelque chose en sortie. Les entrées peuvent être multiples !

***Exemple:***  Conversion centimètre - mètre :
On commence par déclarer la fonction, c'est-à-dire la définir, créer:  
```python
def convertir_en_metre(taille):
    return taille/100
```

Ensuite on l'exécute, et ce par exemple directement dans la console :
```python
>>> convertir_en_metre(180)
1.8
```

* L'entrée est la variable taille (en cm). On déclare une fonction avec la syntaxe: 

```python
def nom_de_la_fonction(parametre):
    #ce que dois faire la fonction
    return #ce que la fonction doit renvoyer
```
* À la fin de la ligne, il faut `:` et une indentation à la ligne suivante. (L'indentation se fait automatiquement la plupart du temps lorsqu'on passe à la ligne après le `:`.)

* La sortie est obtenue avec le mot clé return 


On peut appeler plusieurs fois une fonction avec des paramètres différents, comme en mathématiques.  


***Remarque:*** Les fonctions pouvant vite devenir compliquées, l'usage est d'ajouter un commentaire dans la première ligne de leur définition afin de décrire leur comportement : variables d'entrées, variables de sorties, rôle etc.



# Effets de bord

Contrairement aux fonctions mathématiques, les fonctions en Python peuvent avoir des effets de bord. 

Cela signifie qu'on peut leur demander de faire autre chose que de simplement renvoyer une valeur. 

À vrai dire, certaines fonctions ne renvoient aucune valeur et ne comportent que des effets de bord. 

***Exemple:***
```python
def dire_bonjour(prenom):
    print( "bonjour " + prenom + " !")
```

Cette fonction ne fait pas figurer le mot clé `return`, elle ne renvoie donc aucune valeur. On peut néanmoins l'appeler comme les autres, et ce directement dans la console :

```python
>>> dire_bonjour("Marcel")
bonjour Marcel !
```


# Applications: le périmètre et l'aire d'un rectangle

Nous allons concevoir deux fonctions qui prendront en paramètre les mesures des côtés d'un rectangle.

La première renverra son périmètre, la seconde renverra son aire.  
```python
def perimetre(longueur, largeur):
    return 2 * (longueur + largeur)

>>> perimetre( 2, 5 )
14
```

Le périmètre est $2 + 5 + 2 + 5 = 14$. Tout va bien...  



***À vous de jouer!*** 

Créer la fonction `aire` qui prendra les mêmes paramètres que la fonction `perimetre` et renverra l'aire du rectangle. 
On doit pouvoir l'appeler de la manière suivante :  
```python
>>> aire( 10, 3)
30
```
