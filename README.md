---
title: "Sommaire"
papersize: a4
fontsize: 10pt
author: B. Lenoir
subject: "Markdown"
lang: "fr"
book: true
geometry: margin=2cm
mainfont: times
---

* Vous pouvez accéder aux versions numériques des cours et des TP en cliquant sur les noms correspondants
  
* Pour Les cours une version notebook (interactive) est également proposée en cliquant sur les images  ![my binder](https://mybinder.org/badge_logo.svg).  
Après chargement du notebook vous pouvez exécuter chaque cellule de code en la sélectionnant puis en utilisant la combinaison de touches `ctrl`+`Entrée`


# Chapitre 1: Découverte du langage Python
---
## PARTIE 1: Manipulation du langage Python

---
## PARTIE 2: Fonctions 

* [Cours:  Les fonctions](./python/1_debut_python/1_2_fonctions/cours_fonctions.md)
[![Notebook Fonctions](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fben.lenoir%2Fcours_nsi/master?filepath=python%2F1_debut_python%2F1_2_fonctions%2Fcours_fonctions.ipynb)


* [TP:  Fonctions](./python/1_debut_python/1_2_fonctions/tp_fonctions.md)


# Chapitre 2: Instructions conditionnelles

* [Cours:  Les instructions conditionnelles](./python/2_instructions_conditionnelles/cours_instructions_cond.md)
[![Notebook Instructions conditionnelles](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fben.lenoir%2Fcours_nsi/master?filepath=python%2F2_instructions_conditionnelles%2Fcours_instructions_cond.ipynb)


* [TP:  Fonctions](./python/2_instructions_conditionnelles/tp_instructions_cond.md)

---
# Chapitre 3: Les boucles

## PARTIE 1: Boucles non bornées

* [Cours:  Les boucles non bornées `while`](./python/3_boucles/3_1_boucles_while/cours_boucles_while.md)
[![Notebook Boucles non bornées](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fben.lenoir%2Fcours_nsi/master?filepath=python%2F3_boucles%2F3_1_boucles_while%2Fcours_boucles_while.ipynb)


* [TP1:  Boucles non bornées](./python/3_boucles/3_1_boucles_while/tp_boucles_while.md)

---
## PARTIE 2: Boucles bornées


* [Cours:  Les boucles bornées `for`](./python/3_boucles/3_2_boucles_for/cours_boucles_for.md)
[![Notebook Boucles bornées](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fben.lenoir%2Fcours_nsi/master?filepath=python%2F3_boucles%2F3_2_boucles_for%2Fcours_boucles_for.ipynb)


* [TP2:  Boucles bornées](./python/3_boucles/3_2_boucles_for/tp_turtle.md)

---